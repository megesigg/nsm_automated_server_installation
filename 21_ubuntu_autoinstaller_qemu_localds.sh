### SOURCE: https://ubuntu.com/server/docs/install/autoinstall-quickstart ###


##################################################
#### Vorbereitung Linux-Client
##################################################

### Create working directory
mkdir -p ~/dev/autoinstall
 
### Download latest Ubuntu LTS ServerISO (22.04)
cd ~/dev/autoinstall
wget https://releases.ubuntu.com/jammy/ubuntu-22.04.3-live-server-amd64.iso
 
### Install Software
sudo apt install python3 qemu-system-x86 whois cloud-image-utils


##################################################
#### KVM/QEMU - localds
##################################################

### Create cloud-init datasource directory
mkdir -p ~/dev/autoinstall/cidata

### Crypt Password
mkpasswd --method=sha-512 ubuntu1234
 
### Create answer files (user-data + meta-data)
cd ~/dev/autoinstall/cidata

# -> Replace password hash with output from mkpasswd!
cat > user-data << 'EOF'
#cloud-config
autoinstall:
  version: 1
  identity:
    hostname: ubuntu-server
    password: "$6$AzThmUPjCY1ts8L4$Iwp/m7oFvvMITDRetMBGQ2xDlavT7Es.Sq86RAQq.6CEii0DF6G6hWCqn870WmJz3TE2jdveYYxmmgcPzHB6f."
    username: ubuntu
  keyboard:
    layout: ch
EOF
 
touch meta-data vendor-data
 
### Create an ISO to use as a cloud-init data source
cloud-localds ~/dev/autoinstall/seed.iso user-data meta-data
 
### Create disk file
truncate -s 10G ~/dev/autoinstall/cidata/image.img
 
### Start installation in new Terminal/Tab (Duration: 5-10min)
kvm -no-reboot -m 2048 -cpu host \
    -drive file=~/dev/autoinstall/cidata/image.img,format=raw,cache=none,if=virtio \
    -drive file=~/dev/autoinstall/seed.iso,format=raw,cache=none,if=virtio \
    -cdrom ~/dev/autoinstall/ubuntu-22.04.3-live-server-amd64.iso
 
# Continue with autoinstall? (yes|no) -> yes
 
### Boot the installed system
kvm -no-reboot -m 2048 -cpu host -drive file=~/dev/autoinstall/cidata/image.img,format=raw,cache=none,if=virtio
 
### Login with ubuntu / ubuntu1234