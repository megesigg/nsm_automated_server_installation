### SOURCE: https://ubuntu.com/server/docs/install/autoinstall-quickstart ###


##################################################
#### Vorbereitung Linux-Client
##################################################

### Create working directory
mkdir -p ~/dev/autoinstall
 
### Download latest Ubuntu LTS ServerISO (22.04)
cd ~/dev/autoinstall
wget https://releases.ubuntu.com/jammy/ubuntu-22.04.3-live-server-amd64.iso
 
### Install Software
sudo apt install python3 qemu-system-x86 whois cloud-image-utils


##################################################
#### KVM/QEMU - HTTP
##################################################

### Mount ISO
sudo mount -r ~/dev/autoinstall/ubuntu-22.04.3-live-server-amd64.iso /mnt/
 
### Create http directory
mkdir ~/dev/autoinstall/www
 
### Crypt Password
mkpasswd --method=sha-512 ubuntu1234
 
### Create answer files (user-data + meta-data + vendor-data)
cd ~/dev/autoinstall/www

# -> Replace password hash with output from mkpasswd!
cat > user-data << 'EOF'
#cloud-config
autoinstall:
  version: 1
  identity:
    hostname: ubuntu-server
    password: "$6$AzThmUPjCY1ts8L4$Iwp/m7oFvvMITDRetMBGQ2xDlavT7Es.Sq86RAQq.6CEii0DF6G6hWCqn870WmJz3TE2jdveYYxmmgcPzHB6f."
    username: ubuntu
  keyboard:
    layout: ch
EOF
 
touch meta-data vendor-data
 
### Create disk file
truncate -s 10G ~/dev/autoinstall/image.img
 
### Start HTTP Server
python3 -m http.server 3003 --directory ~/dev/autoinstall/www
 
### Start installation in new Terminal/Tab (Duration: 5-10min)
kvm -no-reboot -m 2048 -cpu host \
    -drive file=~/dev/autoinstall/image.img,format=raw,cache=none,if=virtio \
    -cdrom ~/dev/autoinstall/ubuntu-22.04.3-live-server-amd64.iso \
    -kernel /mnt/casper/vmlinuz \
    -initrd /mnt/casper/initrd \
    -append 'autoinstall ds=nocloud-net;s=http://_gateway:3003/'
 
### Boot the installed system
kvm -no-reboot -m 2048 -cpu host -drive file=~/dev/autoinstall/image.img,format=raw,cache=none,if=virtio
 
### Login with ubuntu / ubuntu1234