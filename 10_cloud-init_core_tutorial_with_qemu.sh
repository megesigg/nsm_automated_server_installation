### SOURCE: https://cloudinit.readthedocs.io/en/latest/tutorial/qemu.html ###


##################################################
#### Core tutorial with QEMU
##################################################

### Create working directory
mkdir -p ~/dev/cloud-init
 
### Download latest Ubuntu LTS ServerISO (22.04)
cd ~/dev/cloud-init
wget https://cloud-images.ubuntu.com/jammy/current/jammy-server-cloudimg-amd64.img
 
### Install Software
sudo apt install python3 qemu-system-x86


### Define user data
cd ~/dev/cloud-init
 
cat << EOF > user-data
#cloud-config
password: password
chpasswd:
  expire: False
packages:
  - apache2
write_files:
- path: /var/www/html/index.html
  content: |
    <html>
      <head>
      </head>
      <body>
        <h1>Hello World!</h1>
      </body>
    </html>
  defer: true
 
EOF
 
### Define metadata
cat << EOF > meta-data
instance-id: someid/somehostname
local-hostname: jammy
 
EOF
 
### Define vendor data
touch vendor-data
 
### Start ad hoc Instance Metadata Service (IMDS) webserver
python3 -m http.server --directory ~/dev/cloud-init
 
### Launch virtual machine in new Terminal/Tab with our user data
qemu-system-x86_64 \
    -net nic -net user,hostfwd=tcp::8080-:80 \
    -machine accel=kvm:tcg \
    -cpu host \
    -m 512 \
    -nographic \
    -hda ~/dev/cloud-init/jammy-server-cloudimg-amd64.img \
    -smbios type=1,serial=ds='nocloud;s=http://10.0.2.2:8000/'
 
### Login with ubuntu / password
 
### Check cloud-init status
cloud-init status --wait

### Getting SMBIOS data
sudo dmidecode -t 1

### Browse http://localhost:8080/